# Windows BATCH scripts



## What is it?
This project contains some tools I put together for my own usage in Windows BATCH. They're by no means complete or ready for any particular use, so you will have to modify them for your needs. Please use them carefully and only if you know what you're doing. Any liability for any damages using the software is not possible.

## What is what?

Pinger: a script to ping an IP on the local network regularly as a (simple, not infinitely reliable, but working) means of detecting uptime. A small MP3 player and a honk sound is included to simplify alerting. Website of the included Audio Player can be found under the link: [https://www.dcmembers.com/skwire/download/swavplayer/](https://www.dcmembers.com/skwire/download/swavplayer/)

More coming soon
